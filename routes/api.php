<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group([
    'prefix' => 'v1',
    'as' => 'api.'
], function ($router) {

    Route::group([
        'prefix' => 'auth',
    ], function ($router) {
        Route::post('register', 'Auth\RegisterController@register')->name('register.post');
        Route::post('login', 'Auth\LoginController@login')->name('login.post');
        Route::get('refresh', 'Auth\LoginController@refresh')->name('login.refresh.token');
        Route::get('user', 'Auth\LoginController@getUser')->name('login.user.get');
        Route::post('logout', 'Auth\LoginController@logout')->name('logout.post');

    });

    Route::group([
        'middleware' => 'auth:api',
        'namespace' => 'Api',
    ], function ($router) {
        //DomainResource Routes
        Route::apiResource('users/domains', 'User\Domain\DomainController')->names([
            'index' => 'user.domains.index',
            'store' => 'user.domains.store',
            'show' => 'user.domains.show',
            'destroy' => 'user.domains.destroy',
        ])->except('create','edit','update');
        //Domain verification (note that we should not use `verify` because its a verb)
        Route::post('users/domains/{domainNotVerified}/verification/', 'User\Domain\VerificationController@store')->name('user.domains.verification.store');

    });
});

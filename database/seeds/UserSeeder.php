<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();
        factory(\App\User::class)->create([
            'name' => 'arashi',
            'email' => 'arash@gmail.com',
            'email_verified_at' => now(),
            'nameservers' => [
                'n.ns.arvancdn.com',
                'e.ns.arvancdn.com',
            ],
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => null,
        ]);
        factory(\App\User::class,5)->create();

    }
}

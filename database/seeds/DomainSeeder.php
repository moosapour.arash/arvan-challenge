<?php

use Illuminate\Database\Seeder;

class DomainSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('domains')->truncate();
        $users = \App\User::all();
        $users->each(function ($user){
            factory(\App\Domain::class,rand(1,3))->create([
                'user_id' => $user->id,
            ]);
            factory(\App\Domain::class,rand(1,3))->states('not_verified')->create([
                'user_id' => $user->id,
            ]);
        });

    }
}

<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Domain;
use Faker\Generator as Faker;

$factory->define(Domain::class, function (Faker $faker) {
    return [
        'user_id' => function () {
            return factory(\App\User::class)->create()->id;
        },
        'domain' => $faker->domainName,
        'verified' => true,
        'verified_at' => now(),
    ];
});

$factory->state(Domain::class, 'not_verified', function (Faker $faker) {
    return [
        'verified' => false,
        'verified_at' => null,
    ];
});

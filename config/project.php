<?php

return [
   'arvan_name_servers' => [
       'a.ns.arvancdn.com',
       'b.ns.arvancdn.com',
       'c.ns.arvancdn.com',
       'd.ns.arvancdn.com',
       'e.ns.arvancdn.com',
       'f.ns.arvancdn.com',
       'g.ns.arvancdn.com',
       'h.ns.arvancdn.com',
       'i.ns.arvancdn.com',
       'j.ns.arvancdn.com',
       'k.ns.arvancdn.com',
       'l.ns.arvancdn.com',
       'm.ns.arvancdn.com',
       'n.ns.arvancdn.com',
       'o.ns.arvancdn.com',
       'p.ns.arvancdn.com',
       'q.ns.arvancdn.com',
       'r.ns.arvancdn.com',
       's.ns.arvancdn.com',
       't.ns.arvancdn.com',
       'u.ns.arvancdn.com',
       'v.ns.arvancdn.com',
       'w.ns.arvancdn.com',
       'x.ns.arvancdn.com',
       'y.ns.arvancdn.com',
       'z.ns.arvancdn.com',
   ]
];

<?php

namespace App\Repositories;


use App\Domain;
use App\User;

class DomainRepository extends AbstractRepository
{
    public function paginate()
    {
        return $this->model::query()->paginate();
    }

    public function create(array $data)
    {
        return $this->model::query()->create($data);
    }

    public function createByUser(User $user,$data)
    {
        return $user->domains()->create($data);
    }

    public function getByUser(User $user)
    {
        return $user->domains()->paginate();
    }

    public function verify(Domain $domain)
    {
        return $domain->update(['verified' => true,'verified_at' => now()]);
    }

    public function deleteNotVerifiedDomains(Domain $domain){
        $this->model::query()->where('domain',$domain->domain)
            ->where('verified',false)->delete();
    }

    public function delete(Domain $domain)
    {
        return $domain->delete();
    }
}

<?php

namespace App\Repositories;


use Illuminate\Database\Eloquent\Model;

class AbstractRepository
{
    /**
     * @var Model
     */
    protected $model;

    /**
     * Repository constructor.
     * @param string $model
     *
     * @throws \ReflectionException
     */
    public function __construct(string $model)
    {
        $reflection = new \ReflectionClass($model);
        if (!$reflection->isSubclassOf(Model::class)) {
            throw new \InvalidArgumentException(sprintf("%s must be instance of %s", $model, Model::class));
        }
        $this->model = $model;
    }


    /**
     * this can be used to call eloquent builders directly on repository
     * @param $method
     * @param $args
     *
     * @return mixed
     */
    public function __call($method, $args)
    {
        return call_user_func_array([$this->model, $method], $args);
    }
}

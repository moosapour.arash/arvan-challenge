<?php


namespace App\Repositories;

class UserRepository extends AbstractRepository
{
    public function paginate()
    {
        return $this->model::query()->paginate();
    }

    public function create(array $data)
    {
        return $this->model::query()->create($data);
    }
}

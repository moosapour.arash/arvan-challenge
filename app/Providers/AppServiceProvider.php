<?php

namespace App\Providers;

use App\Domain;
use App\Repositories\DomainRepository;
use App\Repositories\UserRepository;
use App\Services\Domain\DigDNSResolver;
use App\Services\Domain\DomainDNSValidator;
use App\Services\Domain\NativeDNSResolver;
use App\User;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(UserRepository::class, function ($app) {
            return new UserRepository(User::class);
        });

        $this->app->bind(DomainRepository::class, function ($app) {
            return new DomainRepository(Domain::class);
        });

        $this->app->singleton(DomainDNSValidator::class,NativeDNSResolver::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}

<?php

namespace App\Providers\App\Listeners;

use App\Providers\App\Events\DomainVerified;
use App\Repositories\DomainRepository;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class PruneSimilarNotVerifiedDomains
{
    /**
     * @var \App\Repositories\DomainRepository
     */
    private $repository;

    /**
     * Create the event listener.
     *
     * @param \App\Repositories\DomainRepository $repository
     */
    public function __construct(DomainRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Handle the event.
     *
     * @param  DomainVerified  $event
     * @return void
     */
    public function handle(DomainVerified $event)
    {
        $this->repository->deleteNotVerifiedDomains($event->domain);

    }
}

<?php

namespace App\Providers\App\Events;

use App\Domain;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class DomainVerified
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    /**
     * @var \App\Domain
     */
    public $domain;

    /**
     * Create a new event instance.
     *
     * @param \App\Domain $domain
     */
    public function __construct(Domain $domain)
    {
        $this->domain = $domain;
    }

}

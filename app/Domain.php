<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Domain extends Model
{
    protected $casts =[
      'user_id' => 'integer',
      'verified' => 'boolean',
    ];
    protected $guarded = [];
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function scopeNotVerified($query) :void
    {
        $query->where('verified',false);
    }
}

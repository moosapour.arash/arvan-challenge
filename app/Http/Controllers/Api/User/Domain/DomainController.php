<?php

namespace App\Http\Controllers\Api\User\Domain;


use App\Domain;
use App\Http\Controllers\Controller;
use App\Http\Requests\DomainRequest;
use App\Http\Resources\DomainCollection;
use App\Http\Resources\DomainResource;
use App\Repositories\DomainRepository;
use Illuminate\Http\Request;

class DomainController extends Controller
{

    public function __construct()
    {
        \Auth::shouldUse('api');
        $this->authorizeResource(Domain::class, 'domain');
    }

    /**
     * Display a listing of the resource.
     *
     * @param \App\Repositories\DomainRepository $repository
     *
     * @param \Illuminate\Http\Request           $request
     *
     * @return \App\Http\Resources\DomainCollection
     */
    public function index(DomainRepository $repository,Request $request)
    {
        /** @var \App\User $user */
        $user = $request->user();
        return new DomainCollection($repository->getByUser($user));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\DomainRequest   $request
     *
     * @param \App\Repositories\DomainRepository $repository
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(DomainRequest $request, DomainRepository $repository)
    {
        /** @var \App\User $user */
        $user = $request->user();
        $stored = $repository->createByUser($user, $request->validated());
        return (new DomainResource($stored))
            ->response()
            ->setStatusCode(201)
            ->header('Location',route('api.user.domains.show', $stored));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Domain $domain
     *
     * @return \App\Http\Resources\DomainResource
     */
    public function show(Domain $domain)
    {
        return new DomainResource($domain);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Domain                       $domain
     *
     * @param \App\Repositories\DomainRepository $repository
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Domain $domain, DomainRepository $repository)
    {
        $repository->delete($domain);
        return (new DomainResource($domain))
            ->response()
            ->setStatusCode(200);

    }
}

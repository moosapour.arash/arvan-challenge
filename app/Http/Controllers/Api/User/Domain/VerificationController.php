<?php

namespace App\Http\Controllers\Api\User\Domain;

use App\Domain;
use App\Providers\App\Events\DomainVerified;
use App\Repositories\DomainRepository;
use App\Services\Domain\DomainDNSValidator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class VerificationController extends Controller
{
    public function __construct()
    {
        \Auth::shouldUse('api');
    }

    public function store(Domain $domain, DomainDNSValidator $dns_validator, DomainRepository $repository, Request $request)
    {
        $this->authorize('storeVerify', $domain);
        $user = $request->user();
        if (!$dns_validator->checkNameServers($domain->domain, $user->nameservers)) {
            $current_ns = implode(' , ', $dns_validator->getFetchedNameServers());
            return response($this->error("Name servers are invalid , the current name servers are : $current_ns"), 422);
//            throw ValidationException::withMessages(['domain' => "Name servers are invalid , the current name servers are $current_ns"]);
        }
        $repository->verify($domain);
        event(new DomainVerified($domain));
        return response('',204);
    }
}

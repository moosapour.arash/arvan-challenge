<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class DomainRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        switch ($this->method()) {
            case 'POST' :
                {
                    return [
                        'domain' => ['bail', 'required', new \App\Rules\Domain(), Rule::unique('domains')->where(function ($query) {
                            return $query->where('verified', true)->orWhere('user_id', $this->user('api')->id)->where('verified', false);
                        })]
                    ];
                }
            default :
                break;
        }

    }
}

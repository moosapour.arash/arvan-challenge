<?php

namespace App\Services;

use Illuminate\Support\Arr;

/**
 * This class is responsible pick secure random name server for new users from the pool of available name servers
 *
 * @package \App\Services
 */
class NameServerPicker
{
    public static function getNameServers(int $count = 2)
    {
        $pool = config('project.arvan_name_servers');
        $picks = [];
        while (count($picks) < $count) {
            $random = random_int(0, count($pool) - 1);
            if (in_array($pool[$random], $picks)) {
                continue;
            }
            $picks[] = $pool[$random];
        }
        return $picks;
    }
}

<?php

namespace App\Services\Domain;

interface DomainDNSValidator
{
    public function checkNameServers(string $domain,array $name_servers);
    public function getNameServers(string $domain):array;
    public function getFetchedNameServers(): array ;
}

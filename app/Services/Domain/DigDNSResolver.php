<?php

namespace App\Services\Domain;

use Spatie\Dns\Dns;

/**
 * Class DigDNSResolver
 *
 * @package \App\Services\DomainResource
 */
class DigDNSResolver implements DomainDNSValidator
{

    private $fetched_name_servers;

    public function checkNameServers(string $domain, array $name_servers)
    {
        if (!$results = $this->fetchNameServers($domain)) {
            return false;
        }
        return array_diff($results, $name_servers) ? false : true;
    }

    public function getNameServers(string $domain): array
    {
        return $this->fetchNameServers($domain);
    }

    private function fetchNameServers($domain):array
    {
        $dns = new Dns($domain);
        try {
            $r = $dns->getRecords('NS'); // returns all available dns records
        } catch (\Exception $exception){
            report($exception);
            return [];
        }
        if (! $r ){
            return [];
        }
        $explode =explode("\n",trim($r));
        $matches = [];
        foreach ($explode as $item){
            $ns = [];
            preg_match("!NS\s(.+)\.!",$item,$ns);
            $matches[] = $ns[1];
        }
        return $this->fetched_name_servers = $matches ;
    }

    public function getFetchedNameServers(): array
    {
        return $this->fetched_name_servers ?? [];
    }
}

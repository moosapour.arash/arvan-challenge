<?php

namespace App\Services\Domain;

use Illuminate\Support\Arr;

/**
 * Class NativeDNSResolver
 *
 * @package \App\Services\DomainResource
 */
class NativeDNSResolver implements DomainDNSValidator
{

    private $fetched_name_servers;

    public function checkNameServers(string $domain, array $name_servers)
    {
        if (!$results = $this->fetchNameServers($domain)) {
            return false;
        }
//        $domain_nses = Arr::pluck($results, 'target');
        return array_diff($results, $name_servers) ? false : true;
    }

    public function getNameServers(string $domain): array
    {
        return $this->fetchNameServers($domain);
    }


    private function fetchNameServers($domain):array
    {
        return $this->fetched_name_servers = Arr::pluck(Arr::wrap(dns_get_record($domain, DNS_NS)),'target');

    }

    public function getFetchedNameServers(): array
    {
        return $this->fetched_name_servers ?? [];
    }
}

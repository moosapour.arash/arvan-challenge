<?php

namespace Tests\Unit\Auth;

use App\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;

class AuthenticateTest extends TestCase
{
    use RefreshDatabase;
    private $registered_user;

    public function setUp():void
    {
        parent::setUp();

        $this->registered_user = factory(User::class)->create([
            'name' => 'test name',
            'email' => 'test@email.com',
            'password' => \Hash::make('password')
        ]);
    }
// TODO write validation tests

    /** @test */
    public function a_guest_can_register_and_receive_token()
    {
        $this->withoutExceptionHandling();
        Event::fake();
        $email = 'arash@gmail.com';
       $response = $this->postJson(route('api.register.post'), [
            'name' => 'arash',
            'email' => 'arash@gmail.com',
            'password' => 'password',
            'password_confirmation' => 'password',
        ]);
        Event::assertDispatched(Registered::class, function ($e) use ($email) {
            return $e->user->email === $email;
        });
        $response->assertJsonStructure([
            'access_token',
            'token_type',
            'expires_in'
        ]);
    }

    /** @test*/
    public function a_registered_user_can_log_in()
    {
        $this->withoutExceptionHandling();
        $response = $this->postJson(route('api.login.post'), [
            'email'    => $this->registered_user->email,
            'password' => 'password'
        ])->assertStatus(200);
        $response->assertHeader('Authorization');
    }

    /** @test*/
    public function bad_credential_user_can_not_log_in()
    {
        $response = $this->postJson(route('api.login.post'), [
            'email'    => $this->registered_user->email,
            'password' => 'wrong_password'
        ]);

        $response->assertJsonValidationErrors('email');
    }


    /** @test*/
    public function an_authenticated_user_can_logout()
    {
        $this->withoutExceptionHandling();
        auth('api')->login($this->registered_user);

         $this->postJson(route('api.logout.post'))->assertStatus(200);

    }

    /** @test*/
    public function unauthenticated_user_can_not_logout()
    {
         $this->postJson(route('api.logout.post'))->assertStatus(401);

    }
}

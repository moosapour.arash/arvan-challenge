<?php

namespace Tests\Feature\User\Domain;

use App\Domain;
use App\Http\Resources\DomainResource;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DomainTest extends TestCase
{

    use RefreshDatabase;
    private $registered_user;

    public function setUp(): void
    {
        parent::setUp();

        $this->registered_user = factory(User::class)->create([
            'name' => 'test name',
            'email' => 'test@email.com',
            'password' => \Hash::make('password')
        ]);

        $this->signInApi($this->registered_user);

    }

    /** @test */
    public function guest_cannot_get_domains_index()
    {
        auth('api')->logout();
        $this->getJson(route('api.user.domains.index'))->assertStatus(401);
    }

    /** @test */
    public function auth_user_can_get_his_domains_index()
    {
        $domains = factory(Domain::class, 2)->create([
            'user_id' => $this->registered_user->id
        ]);
        factory(Domain::class, 2)->create();
        $response = $this->getJson(route('api.user.domains.index'));
        $response->assertJsonStructure([
            'data',
            'links',
            'meta'
        ]);
        $this->assertEquals($response->json('data')[0]['domain'], $domains->first()->domain);
        $this->assertCount(2, $response->json('data'));
    }

    /** @test */
    public function guest_cannot_store_domain()
    {
        auth('api')->logout();
        $this->postJson(route('api.user.domains.store'))->assertStatus(401);
    }

    /** @test */
    public function store_domain_required()
    {
        app()->setLocale('en');
//        $this->withoutExceptionHandling();
        $response = $this->postJson(route('api.user.domains.store'), [
            'domain' => null
        ]);
        $response->assertJsonValidationErrors(['domain' => "The domain field is required."]);
    }

    /** @test */
    public function store_domain_must_be_valid_url()
    {
        app()->setLocale('en');
//        $this->withoutExceptionHandling();
        $response = $this->postJson(route('api.user.domains.store'), [
            'domain' => 'wrong_url'
        ]);
        $response->assertJsonValidationErrors(['domain' => "The domain is not a valid root domain name"]);
    }

    /** @test */
    public function store_domain_must_be_unique_verified()
    {
        $domain = factory(Domain::class)->create();
        app()->setLocale('en');
        $response = $this->postJson(route('api.user.domains.store'), [
            'domain' => $domain->domain
        ]);
        $response->assertJsonValidationErrors(['domain' => "The domain has already been taken."]);
    }

    /** @test */
    public function store_domain_must_be_unique_for_same_user_even_notverified()
    {
        $domain = factory(Domain::class)->state('not_verified')->create([
            'user_id' => $this->registered_user->id
        ]);
        app()->setLocale('en');
        $response = $this->postJson(route('api.user.domains.store'), [
            'domain' => $domain->domain
        ]);
        $response->assertJsonValidationErrors(['domain' => "The domain has already been taken."]);

//        now check same domain with another user
        auth('api')->logout();
        auth('api')->login($other_user =$this->createOtherUser());
        $response = $this->postJson(route('api.user.domains.store'), [
            'domain' => $domain->domain,
        ]);
        $response->assertStatus(201);
    }

    /** @test */
    public function auth_can_store_valid_domain()
    {
        $this->withoutExceptionHandling();
        $response = $this->postJson(route('api.user.domains.store'), [
            'domain' => 'google.com'
        ]);
        $this->assertEquals('google.com',$this->registered_user->domains->first()->domain);
        $response->assertJsonStructure([
                "data" ,
            ]);
    }

    /** @test */
    public function auth_can_view_one_of_his_own_domains()
    {
        $own_domain = factory(Domain::class)->create([
            'user_id' => $this->registered_user->id
        ]);
        $this->getJson(route('api.user.domains.show',$own_domain))->assertSee($own_domain->domain);
    }

    /** @test */
    public function auth_can_not_view_others_domains()
    {
        $others_domain = factory(Domain::class)->create();
        $this->getJson(route('api.user.domains.show',$others_domain))->assertStatus(403);
    }

    /** @test */
    public function auth_can_delete_one_of_his_own_domains()
    {
        $own_domain = factory(Domain::class)->create([
            'user_id' => $this->registered_user->id
        ]);
        $this->deleteJson(route('api.user.domains.destroy',$own_domain))->assertStatus(200);
        $this->assertEquals(0,Domain::query()->count());
    }

    /** @test */
    public function auth_cannot_delete_others_domains()
    {
        $others_domain = factory(Domain::class)->create();
        $this->deleteJson(route('api.user.domains.destroy',$others_domain))->assertStatus(403);
        $this->assertEquals(1,Domain::query()->count());
    }
    private function createOtherUser()
    {
        return factory(User::class)->create();
    }


}

<?php

namespace Tests\Feature\User\Domain;

use App\Domain;
use App\Services\Domain\DomainDNSValidator;
use App\User;
use Illuminate\Support\Facades\Cache;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DomainVerifyTest extends TestCase
{

    use RefreshDatabase;
    private $registered_user;
    private $not_verified_domain;

    public function setUp(): void
    {
        parent::setUp();

        $this->registered_user = factory(User::class)->create([
            'name' => 'test name',
            'email' => 'test@email.com',
            'password' => \Hash::make('password')
        ]);
        $this->not_verified_domain = factory(Domain::class)->state('not_verified')->create([
            'user_id' => $this->registered_user->id
        ]);
        $this->signInApi($this->registered_user);

    }

    /** @test */
    public function guest_cannot_verify_domains()
    {
        auth('api')->logout();
        $this->postJson(route('api.user.domains.verification.store', $this->not_verified_domain))->assertStatus(401);
    }

    /** @test */
    public function user_can_not_verify_others_domains()
    {
        $other_domain = factory(Domain::class)->state('not_verified')->create();
        $this->postJson(route('api.user.domains.verification.store', $other_domain))->assertStatus(403);
    }

    /** @test */
    public function user_can_verify_his_own_domain_if_dns_are_correct()
    {
        $this->withoutExceptionHandling();
        $mock = \Mockery::mock(DomainDNSValidator::class);
        $mock->shouldReceive('checkNameServers')->once()->andReturn(true);
        $this->app->instance(DomainDNSValidator::class, $mock);
        $this->postJson(route('api.user.domains.verification.store', $this->not_verified_domain));

        $this->assertEquals(true, $this->not_verified_domain->fresh()->verified);
    }

    /** @test */
    public function user_cannot_verify_his_own_domain_if_dns_are_invalid()
    {
        $this->withoutExceptionHandling();
        $mock = \Mockery::mock(DomainDNSValidator::class);
        $mock->shouldReceive('checkNameServers')->once()->andReturn(false);
        $mock->shouldReceive('getFetchedNameServers')->once()->andReturn(['wrong_ns1','wrong_ns2']);
        $this->app->instance(DomainDNSValidator::class, $mock);
        $response =$this->postJson(route('api.user.domains.verification.store', $this->not_verified_domain))
        ->assertStatus(422);
        $response->assertSee('wrong_ns1');
        $this->assertEquals(false, $this->not_verified_domain->fresh()->verified);
    }

    /** @test */
    public function user_cannot_verify_already_verified_domains()
    {
        $domain = factory(Domain::class)->create([
            'user_id' => $this->registered_user->id
        ]);
        $this->postJson(route('api.user.domains.verification.store', $domain))->assertStatus(404);
    }



    /** @test */
    public function validating_a_domain_deletes_similar_unverified_domains()
    {
        $similar_domain = factory(Domain::class)->state('not_verified')->create([
            'domain' => $this->not_verified_domain->domain
        ]);
        $this->withoutExceptionHandling();
        $mock = \Mockery::mock(DomainDNSValidator::class);
        $mock->shouldReceive('checkNameServers')->once()->andReturn(true);
        $this->app->instance(DomainDNSValidator::class, $mock);
        $this->postJson(route('api.user.domains.verification.store', $this->not_verified_domain));
        $this->assertEquals(1,Domain::query()->where('domain',$similar_domain->domain)->count());
    }


}

<?php

namespace Tests\Unit;

use App\Domain;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DomainTest extends TestCase
{

    use RefreshDatabase;

    /** @test */
    public function a_domain_belongs_to_a_user()
    {
       $domain = factory(Domain::class)->create();
       $this->assertInstanceOf(User::class,$domain->user);
    }
}

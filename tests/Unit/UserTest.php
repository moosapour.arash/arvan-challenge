<?php

namespace Tests\Unit;

use App\Domain;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{

    use RefreshDatabase;

    /** @test */
    public function a_domain_belongs_to_a_user()
    {
       $user = factory(User::class)->create();
        factory(Domain::class)->create([
           'user_id' => $user->id
       ]);
       $this->assertInstanceOf(Domain::class,$user->domains->first());
    }
}
